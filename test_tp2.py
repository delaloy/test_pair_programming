from tp2 import *

def test_box_create():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    assert "truc1" in b
    assert "truc2" in b
    assert "truc3" not in b

def test_box_open():
    b = Box()
    assert b.is_open()== True
    b.close()
    assert b.is_open()== False
    b.open()
    assert b.is_open()== True

def test_action_():
    b = Box()
    assert(b.action_look() == 'La boite contient : truc1 truc2 ')
    b.close()
    assert b.action_look() == "la boite est fermée"
    b.open()
    assert b.action_look()
    
def test_volume():
    c = Objet()
    assert (c.get_volume() == None)
    c.set_volume("volume")
    assert (c.get_volume() == "volume")